0.15	203	_ásmo Gazy
0.16	194	_ídliště Antala Staška
0.25	202	Palestinci si nárokují území _ýchodního Jeruzaléma.
0.29	210	Texas rozhodně neleží na _tředozápadě.
0.29	164	obec Hora _vatého Šebestiána
0.31	180	v _akousko-uherské monarchii
0.31	194	_ídliště Míru
0.32	171	Mongolové opanovali celou _třední Asii.
0.34	189	osada Štrbské _leso
0.38	234	_ídliště Svobody
0.42	186	kostel sv. Michaela _rchanděla
0.42	186	autobusová zastávka Náměstí _ratří Synků
0.43	183	ulice K Nové _si
0.44	177	obec Kašperské _ory
0.44	189	souhvězdí Velké _edvědice
0.45	186	dům U Dvou _lunců
0.45	202	_rkonošské pohoří
0.46	186	Hong_ong
0.46	190	byt na Jižním _ěstě
0.47	196	_indřichohradecký zámek
0.48	189	na území _aldštejnského panství
0.49	169	městská část Královo _ole
0.49	188	celoroční horoskop pro znamení _yb
0.49	197	zastávka _áměstí Svobody
0.51	202	pohřebiště v údolí _rálů
0.51	186	chudina v _orutanském vévodství
0.52	192	Tchaj-_an
0.53	192	_panělský sál na Pražském hradě
0.53	195	Pobřeží _lonoviny
0.54	193	ulice Stará _pitálská
0.54	187	Láska je jako _ečernice.
0.55	190	Dyjsko-_vratecký úval
0.55	193	Hostýnsko-_setínská hornatina
0.55	191	_taroměstské pověsti
0.55	185	_isterciácký klášter
0.55	181	na území _rýdlantského vévodství
0.55	177	Přílivová vlna se žene na celé _olní Povltaví.
0.56	190	_rantiškánský klášter
0.56	181	ulice Horní _eská
0.56	181	_oravské Slovácko
0.56	181	Spojené státy _merické
0.56	185	mniši ze _ázavského kláštera
0.57	170	Heřmanův _ěstec
0.57	197	prohlídka _počenského panství
0.58	186	Tsunami postihne oblast _adní Indie.
0.58	182	ulice _rálovny Žofie
0.58	183	Přestupoval na Nových _adech.
0.58	191	na Náměstí _vobody
0.6	191	Spojené _rabské emiráty
0.61	180	ulice _ratří Čapků
0.61	194	kostel sv. Tomáše _poštola
0.62	190	sídliště Černý _ost
0.62	187	_olní Sasko
0.62	183	_avkazský Elbrus
0.63	184	ve _luneční soustavě
0.63	198	ulice Velká _radební
0.63	188	_pavské Slezsko
0.63	180	Velké _edvědí jezero
0.63	186	kostel Svatého srdce _áně
0.64	187	Na obloze je celoročně vidět _léčná dráha.
0.64	176	_azašské zemljanky
0.65	197	rozhledna Černá _ora
0.65	195	_arocké pláže
0.65	184	Kozí _řbety
0.65	214	ulice Jánský _ršek
0.65	191	_emě vycházejícího slunce
0.65	194	ulice _a Proseku
0.66	177	_vatovítský chrám
0.66	180	stanice metra Náměstí _íru
0.66	213	_anadská provincie
0.66	181	zájmy _ritského impéria
0.66	179	území Markrabství _oravského
0.66	185	Za mraky se ukryl _ěsíc.
0.66	180	Subsaharská _rabská demokratická republika
0.67	200	ulice 5. _větna
0.67	200	válka na Blízkém _ýchodě
0.67	192	bazilika Nanebevzetí Panny Marie _vatoborské
0.67	199	Podle čínského horoskopu je rok _ada.
0.67	179	ve vsi Sedm _vorů
0.67	192	Blues ovládlo celý _merický Jih.
0.68	192	ulice Mezi _ahrádkami
0.68	187	ulice _a Pomezí
0.68	185	ulice U Nového _uchdola
0.68	183	Severní _edový oceán
0.68	174	Kostelec nad Černými _esy
0.68	212	Mariánské _ázně
0.69	202	Francouzské _olynésie
0.69	178	kostel Všech _vatých
0.69	191	_rancouzské Alpy
0.69	185	Zaplavena byla třetina _ošumaví.
0.69	179	ulice Prvního _luku
0.69	179	ulice K Novému _pořilovu
0.69	215	třída Obránců _íru
0.69	180	Český _áj
0.7	191	_ázně Poděbrady
0.7	192	rázovitý _lezský lid
0.7	193	kostel _rdce Páně
0.71	191	_stecká demonstrace
0.71	181	na území _rotektorátu Čechy a Morava
0.71	188	na Lysé _oře
0.71	178	v okrese Praha-_ýchod
0.72	186	hlavní město Černé _ory
0.72	184	_everní Karolina
0.72	195	_etohrádek Mitrovských
0.72	174	hora Velký Zvon v Českém _ese
0.72	182	_frický stát Burkina Faso
0.72	175	ulice U Kamenné _lavy
0.72	193	Finsku se říká _emě tisíce jezer.
0.72	186	zámek v Novém _ěstě nad Metují
0.72	208	boj na Bílé _oře
0.72	162	Ve _estfálsku byl uzavřen mír.
0.73	193	Jezero Popradského plesa leží v Mengusově _olině.
0.73	179	_everní Rusko
0.73	185	planina Náhorního _arabachu
0.73	185	výzdoba _ladislavského sálu
0.73	195	_ys Dobré naděje
0.73	188	chrám sv. Barbory v Kutné _oře
0.73	188	Krušné _ory
0.73	196	hrad _eský Šternberk
0.74	185	povídky o Malé _traně
0.74	201	Hlavní město leží v Pražské _otlině.
0.74	198	_védské zastupitelství
0.74	196	_ytířský sál Valdštejnského paláce
0.74	177	ulice Panský _vět
0.74	170	Velikonoční ostrov, tedy Rapa _ui
0.75	194	ve _třední Evropě
0.75	169	Vzali nám _odkarpatskou Rus.
0.75	183	městská část Brno-_třed
0.76	188	Do oken svítila _una.
0.76	176	_hrám sv. Jakuba
0.76	172	Klíčem k Latinské _merice je Panama.
0.76	193	Malá Studená _otlina
0.76	177	Velký _ariérový útes
0.76	182	propasti Moravského _rasu
0.76	187	náměstí Organizace _pojených národů
0.76	207	Na _elhřimovsku byl spatřen rys.
0.76	186	ulice U Místní _ráhy
0.77	185	v Králové_radeckém kraji
0.77	183	krkonošský Železný _rch
0.77	188	republika _vatá Lucie
0.77	193	Nad obzor vychází ranní _lunce.
0.77	203	_arrandovský most
0.77	181	ulice Na _taré vinici
0.77	204	kostel _anny Marie u alžbětinek
0.77	191	kostel _anny Marie
0.78	201	Nejvíce stiženo je _rálovéhradecko.
0.78	197	kaple _ožího milosrdenství
0.78	185	_alý sál Městské knihovny v Praze
0.78	199	ulice K Novému _ubenči
0.78	190	New York _ity
0.78	181	_etohrádek Belvedere
0.78	195	_ízké Tatry
0.78	186	Veľké Hincovo _leso
0.78	205	V _odyjí najdeme nejmenší národní park.
0.78	203	ulice K _ybníčku
0.78	186	Spojené království Velké Británie a _everního Irska
0.78	186	prezident _elké Kolumbie
0.79	187	náměstí 28. _íjna
0.79	188	_aldštejnský sál
0.79	179	v _anamském průplavu
0.79	198	hora Malý _išák
0.79	187	vystoupali na Černou _oru
0.79	183	_elký sál Rudolfina
0.79	188	Pražský _rad
0.79	175	_lzeňská dálnice
0.79	185	ulice Pražského _ovstání
0.79	185	_ábřeží Bedřicha Smetany
0.8	202	vznik života na _emi
0.8	188	ulice Nad _yhlídkou
0.8	177	kostel sv. Václava na _deraze
0.8	198	v _adech Pětatřicátníků
0.8	198	Petrova _ouda na hoře Dívčí kameny
0.81	195	_etohrádek Hvězda na Bílé hoře
0.81	201	magnetické pole _emě
0.81	207	_áměstí Prezidenta Masaryka
0.81	192	náměstí Pod _aštany
0.81	199	zámek Červená _hota
0.81	194	_ísecký starosta
0.81	189	Hornomoravský _val
0.81	185	kostel Andělů _trážných
0.81	193	Celý _yrenejský poloostrov zaplavili přistěhovalci.
0.81	194	sdružení obcí _ikroregionu Bystřička
0.81	189	summit v Saudské _rábii
0.82	194	ulice K _anenkám
0.82	183	Nová _aledonie
0.82	189	Andorra _a Vella
0.82	195	Hrochův _ýnec
0.82	201	Při formování naší galaxie hrálo hlavní roli _lunce.
0.82	190	dům _ Tří kohoutů
0.82	174	Rusko je baštou _ýchodní Evropy.
0.82	186	náměstí Nad _erasou
0.82	181	Králický _něžník
0.82	198	_řída Míru
0.82	188	_ady Pionýrů
0.83	196	území _alopolska
0.83	199	_ibeňský most
0.83	189	_ys Horn
0.83	187	ulice Malá _těpánská
0.83	181	_ídliště Líšeň
0.84	196	Salt Lake _ity
0.84	196	_rněnská dálnice
0.84	190	v _adu
0.84	176	v _acifiku
0.84	189	_ižní Korea
0.84	184	_ikroregion Olešovnicko
0.84	204	Kapky dopadají na rozměklou _emi.
0.84	173	_urecký tabák
0.84	174	Svatá _íše římská
0.85	194	v El _asu
0.85	198	Jiráskovo _ábřeží
0.85	192	sraz na Tyršově _ostě
0.85	188	v _echách
0.85	188	_lovenské sýry
0.85	189	_ila Tugendhat
0.85	196	Drahanská _rchovina
0.85	191	v Janáčkově _leji
0.86	207	oceánský stát Cookovy _strovy
0.86	166	oblast _orúří
0.86	194	_uselský most
0.86	203	Kostelec u _řížků
0.86	197	mikroregion Labské _kály
0.86	190	svazek obcí _ovostrašecko
0.86	192	Palestinská _utonomie
0.86	185	_ídliště Starý lískovec
0.86	193	pohled do Ráčkovské _oliny
0.86	193	ulice Za _růhonem
0.86	181	Horní _elení
0.86	204	bazilika _v. Jiří
0.86	175	_ila Stiassni
0.86	197	Tichý _ceán
0.86	190	v _atagonii
0.86	198	mít dům na _alackého
0.86	198	Rychlebské _ory
0.86	192	ulice Při _lumoči
0.87	188	ve Spolkové _epublice Německo
0.87	181	_řída Kapitána Jaroše
0.87	182	Obecní _ům v Praze
0.87	207	Praha-_obylisy
0.87	185	ulice K Lesnímu _ivadlu
0.87	179	náměstí Chuchelských _ojovníků
0.87	181	Rakousko-_hersko
0.87	198	Bílý _ům ve Washingtonu
0.88	192	_aškarní sál Domu kultury
0.88	203	ulice Na _ůstku
0.88	187	dům U _inuty
0.88	189	zahrada Na _alech
0.88	198	Středoafrická _epublika
0.88	200	Dánské _rálovství
0.88	203	Jihoafrická _epublika
0.88	188	_strov Svaté Heleny
0.88	181	Prachovské _kály
0.88	191	dovolená v Guinea-_issau
0.89	192	Středozemní _oře
0.89	175	ostrovní stát Svatý Tomáš a Princův _strov
0.89	185	ulice V _linkách
0.89	186	_álnice D1
0.89	187	svahy Velkého _línovce
0.89	198	v Baden-_adenu
0.89	202	v Ulici _tuchlého
0.89	193	_asáž Jalta
0.89	186	Jel jsem do Jižní _meriky.
0.89	188	v Dominikánské _epublice
0.89	188	Na _ihu Evropy bude deštivo.
0.89	179	Julské _lpy
0.9	191	vlak z Ostravy do Golčova _eníkova
0.9	201	_strov Jáva
0.9	192	_otunda Máří Magdaleny
0.9	213	Divišova _tvrť
0.9	204	Niagarské _odopády
0.9	186	Střelkový _ys
0.9	186	ulice Na _ahálce
0.9	199	_elezniční zastávka Třebechovice pod Orebem
0.9	170	boje ve Východním _imoru
0.9	193	Denisovy _ady
0.9	196	Nezvalova _řída
0.9	186	Moravskéslezské _eskydy
0.9	187	kaple _v. Kříže
0.91	200	pluli po _rinoku
0.91	179	Hrubý _eseník
0.91	190	Stará _adnice v Brně
0.91	191	ulice Na _rádku
0.91	213	park Na _idlovačce
0.91	192	_ál Sladkovského
0.91	193	náměstí Na _antince
0.91	184	Rosice _ Brna
0.91	174	kostel _v. Rodiny
0.91	185	_alác Kinských
0.91	194	kostel sv. Petra na _oříčí
0.91	183	stromy na Pražské _řídě
0.91	183	_alác Šlechtičen
0.91	185	knihovna Strahovského _láštera
0.91	197	v ulici _ápotoční
0.92	177	vodní toky Malý _unaj a Čierna voda
0.92	177	na _krese Brno-venkov
0.92	203	_lice Jindřicha Šimona Baara
0.92	180	Na _ápadě země zuří bouřky.
0.92	193	Suezský _růplav
0.92	182	Václavská _asáž
0.92	182	_ostel sv. Tomáše v Brně
0.92	182	Frýdek-_ístek
0.92	183	Dánsko se nachází na _utském poloostrově.
0.92	184	Chrám _v. Víta v Praze
0.92	198	_rad Karlštejn
0.92	175	ulice U Botanické _ahrady
0.92	201	_strov Svatého Tomáše
0.92	189	palác _oruna
0.92	190	příměří se Severním _yprem
0.92	190	_asáž Rozkvět
0.92	191	Tylova _tvrť
0.92	204	_ámek Velké Losiny
0.92	179	ostrov Isla _e Margarita
0.92	168	Freiburg im _reisgau
0.92	183	Bartošovice v Orlických _orách
0.92	184	Nejkrásnějšími plachetnicemi se chlubí _ichigan.
0.92	211	Klostermannova _ozhledna
0.92	198	beskydská hora _mrk
0.92	172	_ámek Zruč nad Sázavou
0.93	187	kostel _v. Petra a Pavla
0.93	201	Papua-Nová _uinea
0.93	188	kostel sv. Vavřince _od Petřínem
0.93	188	rotunda _v. Víta
0.93	189	Moravský _rumlov
0.93	178	Demokratická _epublika kongo
0.93	192	v jižním _osovu
0.93	180	v Resslově _lici
0.93	194	v Hájkově _lici
0.93	195	Bílé _arpaty
0.93	182	Lidická _lice
0.93	196	hranice Jižního _údánu
0.93	182	_asáž Černá růže
0.93	199	Kalifornský _áliv
0.93	171	státy _aribiku
0.93	186	Baffinův _strov
0.93	188	Největším měsícem _aturnu je Titan.
0.93	190	Washington D. _.
0.93	179	_ámek Hrubý Rohozec
0.93	195	_ohoří Krkonoše
0.93	197	_unel Blanka
0.94	200	_elezniční stanice Brno hlavní nádraží
0.94	185	Kostelec _ad Orlicí
0.94	191	řeka _vratka
0.94	191	ulice Jára _a Cimrmana
0.94	176	Nové _kotsko
0.94	192	_ark Lužánky
0.94	194	ulice Bratří _ašínů
0.94	178	ulice Jiřího _ Poděbrad
0.94	183	Valdštejnský _alác
0.94	200	na celé _oravě
0.94	167	na Malinovského _áměstí
0.94	188	v Betlémské _apli
0.94	190	Veletržní _alác
0.94	192	Frankfurt _ad Mohanem
0.94	176	městská část _abovřesky
0.94	194	vojáci ze Sierra _eone
0.94	177	_elezniční stanice Praha-Smíchov
0.94	197	Müllerova _ila
0.95	182	Reichenbašské _odopády, kde zemřel Sherlock Holmes
0.95	202	San _arino
0.95	190	_ozhledna Kohout
0.95	192	Nejvíce požárů hlásí _raj Olomoucký.
0.95	175	Polabská _ížina
0.95	195	Průmyslový _alác
0.95	198	na _iffelově věži
0.95	179	Nejjasnější hvězdou Velkého psa je _irius.
0.95	183	hranice _rancie
0.95	185	Staroměstská _adnice
0.95	206	Nejmenší planetou naší soustavy je _erkur.
0.95	186	na území Bosny a _ercegoviny
0.95	190	v _elanésii
0.95	192	na _eychelách
0.95	192	Šluknovský _ýběžek
0.95	192	ulice Jana _ucemburského
0.95	193	v _olsku
0.95	193	Frenštát _od Radhoštěm
0.95	174	Západní _řeh Jordánu
0.96	178	Jurkovičova _ila
0.96	178	hrad Ledeč _ad Sázavou
0.96	188	městská _tvrť Bohunice
0.96	193	ostrovy _olynésie
0.96	193	Největším kontinentem je _sie.
0.96	194	zřícenina _radu Chřenovice
0.96	195	Ústí nad _abem
0.96	197	z Jamajky na _ubu
0.96	199	Karlovy _ary
0.96	177	Trinidad a _obago
0.96	184	Mont _lanc
0.96	190	Černé jezero na _umavě
0.97	172	Zruč nad _ázavou
0.97	179	Nová _uinea
0.97	209	Velké _arlovice
0.97	190	rytíři z _laníku
0.97	193	_ozhledna Kozákov
0.97	196	ostrov _orneo
0.97	183	Nový _éland
0.97	187	starosta města _řebíč
0.98	173	St. _ouis
0.98	296	Miluji New _ork.
0.98	193	v _krese Nymburk
0.99	164	České _udějovice
